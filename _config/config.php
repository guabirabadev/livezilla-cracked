<?php

/*********************************************************************************
* LiveZilla config.php
*
* Copyright 2016 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
********************************************************************************/

$_CONFIG['gl_pr_cr'] = '1502460204';
$_CONFIG['gl_lzid'] = '6e55488d6f';
$_CONFIG['b64'] = false;

$_CONFIG[0]['gl_db_host'] = 'localhost';
$_CONFIG[0]['gl_db_user'] = 'root';
$_CONFIG[0]['gl_db_ext'] = 'mysqli';
$_CONFIG[0]['gl_db_eng'] = 'MyISAM';
$_CONFIG[0]['gl_db_pass'] = 'root';
$_CONFIG[0]['gl_db_name'] = 'chatv3';
$_CONFIG[0]['gl_db_prefix'] = 'lz_';

?>